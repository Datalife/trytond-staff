# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView
from trytond.model import ModelSingleton
from trytond.modules.company.model import CompanyMultiValueMixin


class Configuration(ModelSingleton, ModelSQL, ModelView,
            CompanyMultiValueMixin):
    """Staff configuration"""
    __name__ = 'staff.configuration'
